from csv import reader
import matplotlib.pyplot as plt

class Iris:
    def __init__(self):
        self.data_file = reader(open('fono_hortencia'), delimiter='\t')
        self.data=[linha for linha in self.data_file]
        self.setosa = [linha for linha in self.data if 'setosa' in linha[-1]]
        self.versicolor = [linha for linha in self.data if 'versicolor' in linha[-1]]
        self.virginica= [linha for linha in self.data if 'virginica' in linha[-1]]

        self.ydata = list(zip(*self.data))
        self.yset = list(zip(*self.setosa))
        self.yver = list(zip(*self.versicolor))
        self.yvir = list(zip(*self.data))
        print(self.data)

    def plot(self):
        plt.scatter(self.ydata[0],self.ydata[1], color= 'red')
        plt.title("Grafico Scatterplot")
        plt.show()